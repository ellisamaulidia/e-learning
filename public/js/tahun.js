function dataTahun()
{
	
	$('title').text('Mata Kuliah');

	link('#link-data-tahun');

	$('.konten').load(url_data_tahun, function()
	{
		properti();
	});

}

function modalTambahTahun()
{
	$('.modalshow').load(url_tambah_tahun, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});	
}

function tambahTahun(){
	$('.form-tambah-tahun').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nim)
				{
					$('#control-tahun').removeClass('info').addClass('error');
					$('#error-tahun').text(r.nama);
				} else {
					$('#control-tahun').removeClass('error').addClass('info');
					$('#error-tahun').text('');
				}			
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataTahun();
			};		
		}
	});
}

function editTahun()
{
	$('.form-edit-tahun').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nip)
				{
					$('#control-tahun').removeClass('info').addClass('error');
					$('#error-tahun').text(r.nim);
				} else {
					$('#control-tahun').removeClass('error').addClass('info');
					$('#error-tahun').text('');
				};
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataTahun();
			};		
		}
	});
}

function modalEditTahun(id){
	$('.modalshow').load(url_edit_tahun + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function modalHapusTahun(id){
	$('.modalshow').load(url_hapus_tahun + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function hapusTahun(id){
	$.post(url_hapus_tahun + '/' + id, { _token:token }, function(r)
	{
		$('.modal').modal('hide');
		$('.modal').html('');
		//notif('Data barang berhasil dihapus.', 'info');
		dataTahun();
	});
}