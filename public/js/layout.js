$(function()
{
	//Messenger({ maxMessages:1 });
});


function logout()
{
	$.get(url_logout, function()
	{
		$(location).prop('href', url_login);
	});
}

function link(target)
{
	$('.navbar-inner').find('li').removeClass('active');
	$(target).addClass('active');
}

function notif(pesan, tipe)
{
	Messenger().post({
		message:pesan,
		type:tipe,
		showCloseButton:true
	});
}

function ceklis(t)
{
	$('.table').find(':checkbox').prop('checked', $(t).prop('checked'));
}

function paginasi(t)
{
	$('.konten').load($(t).data('target'), function()
	{
		properti();
	});
}

function properti()
{
	$('.tip').tooltip();

	$('.tanggal').datepicker({
		format: 'yyyy-m-d',
		autoclose: true,
		language: 'id'
	});

	$('.type').typeahead({
		items: 5
	});

	$('.pagination a').click(function()
	{
		paginasi(this);
	});
}

$('.modal').on('shown', function()
{
	$('.input-focus').focus();
});


function modalUbahFoto()
{
	$('.modal').load(url_ubah_foto, function()
	{
		$('.modal').modal('show');
	});
}

function ubahFoto()
{
	$('.form-ubah-foto').ajaxSubmit({
		success : function(r)
		{
			if (r.foto)
			{
				$('#control-foto').removeClass('info').addClass('error');
				$('#error-foto').text(r.foto);
			} else {
				$('.modal').modal('hide');
				notif('Foto anda berhasil diubah.', 'info');
			};
		}
	});
}

function modalUbahNama()
{
	$('.modal').load(url_ubah_nama, function()
	{
		$('.modal').modal('show');
	});
}

function enterUbahNama(k)
{
	if (k.which == 13) ubahNama();
}

function ubahNama()
{
	var nama = $('#nama').val().trim();

	$.post(url_ubah_nama, { nama:nama, _token:token }, function(r)
	{
		if (r.nama)
		{
			$('#control-nama').removeClass('info').addClass('error');
			$('#error-nama').text(r.nama);
		} else {
			$('.modal').modal('hide');
			$('.modal').html('');
			$('.user').text(nama);
			notif('Nama anda berhasil diubah.', 'info');
		};
	});
}

function modalUbahUsername()
{
	$('.modal').load(url_ubah_username, function()
	{
		$('.modal').modal('show');
	});
}

function enterUbahUsername(k)
{
	if (k.which == 13) ubahUsername();
}

function ubahUsername()
{
	var username_sekarang = $('#username_sekarang').val().trim();
	var username_baru = $('#username_baru').val().trim();
	var konfirmasi_username = $('#konfirmasi_username').val().trim();

	$.post(url_ubah_username, { username_sekarang:username_sekarang, username_baru:username_baru, konfirmasi_username:konfirmasi_username, _token:token }, function(r)
	{
		if (r.status == '')
		{
			if (r.username_sekarang)
			{
				$('#control-username-sekarang').removeClass('info').addClass('error');
				$('#error-username-sekarang').text(r.username_sekarang);
			} else {
				$('#control-username-sekarang').removeClass('error').addClass('info');
				$('#error-username-sekarang').text('');
			};

			if (r.username_baru)
			{
				$('#control-username-baru').removeClass('info').addClass('error');
				$('#error-username-baru').text(r.username_baru);
			} else {
				$('#control-username-baru').removeClass('error').addClass('info');
				$('#error-username-baru').text('');
			};

			if (r.konfirmasi_username)
			{
				$('#control-konfirmasi-username').removeClass('info').addClass('error');
				$('#error-konfirmasi-username').text(r.konfirmasi_username);
			} else {
				$('#control-konfirmasi-username').removeClass('error').addClass('info');
				$('#error-konfirmasi-username').text('');
			};
		} else {
			$('.modal').modal('hide');
			$('.modal').html('');
			notif('Username anda berhasil diubah.', 'info');
		};
	});
}

function modalUbahPassword()
{
	$('.modal').load(url_ubah_password, function()
	{
		$('.modal').modal('show');
	});
}

function enterUbahPassword(k)
{
	if (k.which == 13) ubahPassword();
}

function ubahPassword()
{
	var password_sekarang = $('#password_sekarang').val().trim();
	var password_baru = $('#password_baru').val().trim();
	var konfirmasi_password = $('#konfirmasi_password').val().trim();

	$.post(url_ubah_password, { password_sekarang:password_sekarang, password_baru:password_baru, konfirmasi_password:konfirmasi_password, _token:token }, function(r)
	{
		if (r.status == '')
		{
			if (r.password_sekarang)
			{
				$('#control-password-sekarang').removeClass('info').addClass('error');
				$('#error-password-sekarang').text(r.password_sekarang);
			} else {
				$('#control-password-sekarang').removeClass('error').addClass('info');
				$('#error-password-sekarang').text('');
			};

			if (r.password_baru)
			{
				$('#control-password-baru').removeClass('info').addClass('error');
				$('#error-password-baru').text(r.password_baru);
			} else {
				$('#control-password-baru').removeClass('error').addClass('info');
				$('#error-password-baru').text('');
			};

			if (r.konfirmasi_password)
			{
				$('#error-konfirmasi-password').text(r.konfirmasi_password);
			} else {
				$('#error-konfirmasi-password').text('');
			};

			$('.modal').find('.control-group').addClass('error');
			$('.modal').find('input').val('');
		} else {
			$('.modal').modal('hide');
			$('.modal').html('');
			notif('Password anda berhasil diubah.', 'info');
		};
	});
}