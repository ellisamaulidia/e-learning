function dataMahasiswa()
{
	
	$('title').text('Data Mahasiswa');

	link('#link-data_mahasiswa');

	$('.konten').load(url_data_mhs, function()
	{
		properti();
	});

}

function modalTambahMhs()
{
	$('.modalshow').load(url_tambah_mhs, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});	
}

function tambahMhs(){
	$('.form-tambah-mhs').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nim)
				{
					$('#control-nim').removeClass('info').addClass('error');
					$('#error-nim').text(r.nama);
				} else {
					$('#control-nim').removeClass('error').addClass('info');
					$('#error-nim').text('');
				}			
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMahasiswa();
			};		
		}
	});
}

function editMhs()
{
	$('.form-edit-mhs').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nim)
				{
					$('#control-nim').removeClass('info').addClass('error');
					$('#error-nim').text(r.nim);
				} else {
					$('#control-nim').removeClass('error').addClass('info');
					$('#error-nim').text('');
				};
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMahasiswa();
			};		
		}
	});
}

function modalEditMhs(id){
	$('.modalshow').load(url_edit_mhs + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function modalHapusMhs(id){
	$('.modalshow').load(url_hapus_mhs + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function hapusMhs(id){
	$.post(url_hapus_mhs + '/' + id, { _token:token }, function(r)
	{
		$('.modal').modal('hide');
		$('.modal').html('');
		//notif('Data barang berhasil dihapus.', 'info');
		dataMahasiswa();
	});
}