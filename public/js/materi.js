function dataMateri()
{
	
	$('title').text('Data Materi Kuliah');

	link('#link-data_materi');

	$('.konten').load(url_data_materi, function()
	{
		properti();
	});

}

function modalTambahMateri()
{
	$('.modalshow').load(url_tambah_materi, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});	
}

function tambahMateri(){
	$('.form-tambah-materi').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nama_matkul)
				{
					$('#control-nama_matkul').removeClass('info').addClass('error');
					$('#error-nama_matkul').text(r.nama);
				} else {
					$('#control-nama_matkul').removeClass('error').addClass('info');
					$('#error-nama_matkul').text('');
				}			
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMateri();
			};		
		}
	});
}

function editMateri()
{
	$('.form-edit-materi').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nip)
				{
					$('#control-nama_matkul').removeClass('info').addClass('error');
					$('#error-nama_matkul').text(r.nim);
				} else {
					$('#control-nama_matkul').removeClass('error').addClass('info');
					$('#error-nama_matkul').text('');
				};
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMateri();
			};		
		}
	});
}

function modalEditMateri(id){
	$('.modalshow').load(url_edit_materi + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function modalHapusMateri(id){
	$('.modalshow').load(url_hapus_materi + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function hapusMateri(id, tahun){
	$.post(url_hapus_materi + '/' + id, { _token:token }, function(r)
	{
		$('.modal').modal('hide');
		$('.modal').html('');
		//notif('Data barang berhasil dihapus.', 'info');
		dataMateri();
	});
}