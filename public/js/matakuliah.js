function dataMatkul(tahun)
{
	
	$('title').text('Data Matakuliah');

	link('#link-data_matkul');

	$('.konten').load(url_data_matkul + '/' + tahun, function()
	{
		properti();
	});

}

function modalTambahMatkul(tahun)
{
	$('.modalshow').load(url_tambah_matkul + '/' + tahun, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});	
}

function tambahMatkul(tahun){
	$('.form-tambah-matkul').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nama_matkul)
				{
					$('#control-nama_matkul').removeClass('info').addClass('error');
					$('#error-nama_matkul').text(r.nama);
				} else {
					$('#control-nama_matkul').removeClass('error').addClass('info');
					$('#error-nama_matkul').text('');
				}			
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMatkul(tahun);
			};		
		}
	});
}

function editMatkul(tahun)
{
	$('.form-edit-matkul').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nip)
				{
					$('#control-nama_matkul').removeClass('info').addClass('error');
					$('#error-nama_matkul').text(r.nim);
				} else {
					$('#control-nama_matkul').removeClass('error').addClass('info');
					$('#error-nama_matkul').text('');
				};
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataMatkul(tahun);
			};		
		}
	});
}

function modalEditMatkul(id){
	$('.modalshow').load(url_edit_matkul + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function modalHapusMatkul(id){
	$('.modalshow').load(url_hapus_matkul + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function hapusMatkul(id, tahun){
	$.post(url_hapus_matkul + '/' + id, { _token:token }, function(r)
	{
		$('.modal').modal('hide');
		$('.modal').html('');
		//notif('Data barang berhasil dihapus.', 'info');
		dataMatkul(tahun);
	});
}