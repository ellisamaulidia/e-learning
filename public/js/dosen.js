function dataDosen()
{
	
	$('title').text('Data Dosen');

	link('#link-data_dosen');

	$('.konten').load(url_data_dosen, function()
	{
		properti();
	});

}

function modalTambahDosen()
{
	$('.modalshow').load(url_tambah_dosen, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});	
}

function tambahDosen(){
	$('.form-tambah-dosen').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nim)
				{
					$('#control-nip').removeClass('info').addClass('error');
					$('#error-nip').text(r.nama);
				} else {
					$('#control-nip').removeClass('error').addClass('info');
					$('#error-nip').text('');
				}			
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataDosen();
			};		
		}
	});
}

function editDosen()
{
	$('.form-edit-dosen').ajaxSubmit({
		success: function(r)
		{		
			if (r.status == '')
			{			
				if (r.nip)
				{
					$('#control-nip').removeClass('info').addClass('error');
					$('#error-nip').text(r.nim);
				} else {
					$('#control-nip').removeClass('error').addClass('info');
					$('#error-nip').text('');
				};
			} else {
				$('.modal').modal('hide');
				$('.modal').html('');
				dataDosen();
			};		
		}
	});
}

function modalEditDosen(id){
	$('.modalshow').load(url_edit_dosen + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function modalHapusDosen(id){
	$('.modalshow').load(url_hapus_dosen + '/' + id, function()
	{
		$('.modal').modal({
		    show: true,
		    remote: ''
		});
	});
}

function hapusDosen(id){
	$.post(url_hapus_dosen + '/' + id, { _token:token }, function(r)
	{
		$('.modal').modal('hide');
		$('.modal').html('');
		//notif('Data barang berhasil dihapus.', 'info');
		dataDosen();
	});
}