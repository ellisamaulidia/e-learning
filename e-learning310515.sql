-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2015 at 12:48 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `e-learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
`ID` int(11) NOT NULL,
  `KODE_DOSEN` char(10) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `NAMA_DOSEN` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`ID`, `KODE_DOSEN`, `USER_ID`, `NAMA_DOSEN`) VALUES
(2, '796976', 3, 'Ani R');

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
`ID_FORUM` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL,
  `NAMA_FORUM` varchar(50) NOT NULL,
  `CREATED_DATE` datetime NOT NULL,
  `EDITED_DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_quiz`
--

CREATE TABLE IF NOT EXISTS `hasil_quiz` (
  `ID_QUIZ` int(11) NOT NULL,
  `NRP` int(11) NOT NULL,
  `NILAI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
`ID_INFORMASI` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL,
  `NAMA_INFORMASI` varchar(50) NOT NULL,
  `KETERANGAN` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `KODE_KELAS` char(7) NOT NULL,
  `KODE_MATAKULIAH` int(11) DEFAULT NULL,
  `KODE_DOSEN` int(11) NOT NULL,
  `JADWAL_HARI` int(11) NOT NULL,
  `JADWAL_JAM` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas_mahasiswa`
--

CREATE TABLE IF NOT EXISTS `kelas_mahasiswa` (
  `NRP` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
`ID_KOMENTAR` int(11) NOT NULL,
  `ID_FORUM` int(11) NOT NULL,
  `KOMENTAR` text NOT NULL,
  `CREATED_DATE` datetime NOT NULL,
  `EDITED_DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
`ID` int(11) NOT NULL,
  `NRP` char(7) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `NAMA_MAHASISWA` varchar(50) NOT NULL,
  `TAHUN_MASUK` char(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`ID`, `NRP`, `USER_ID`, `NAMA_MAHASISWA`, `TAHUN_MASUK`) VALUES
(1, '78686', 6, 'Ihsan H F', '2013');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
`ID` int(11) NOT NULL,
  `KODE_MATAKULIAH` char(10) NOT NULL,
  `ID_TAHUN` int(11) DEFAULT NULL,
  `MATAKULIAH` varchar(50) NOT NULL,
  `SKS` int(11) NOT NULL,
  `DESKRIPSI` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`ID`, `KODE_MATAKULIAH`, `ID_TAHUN`, `MATAKULIAH`, `SKS`, `DESKRIPSI`) VALUES
(5, 'mk001', 2, 'Sistem Informasi', 3, 'Pengantar'),
(6, 'mk002', 2, 'Pemrograman', 4, '-');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
`ID_QUIZ` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL,
  `NAMA_QUIZ` varchar(50) NOT NULL,
  `TANGGAL_QUIZ` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
`ID_SOAL` int(11) NOT NULL,
  `ID_QUIZ` int(11) NOT NULL,
  `SOAL` text NOT NULL,
  `GAMBAR_SOAL` varchar(25) DEFAULT NULL,
  `SOAL_1` varchar(50) DEFAULT NULL,
  `SOAL_2` varchar(50) DEFAULT NULL,
  `SOAL_3` varchar(50) DEFAULT NULL,
  `SOAL_4` varchar(50) DEFAULT NULL,
  `SOAL_5` varchar(50) DEFAULT NULL,
  `TIPE` int(11) NOT NULL,
  `JAWABAN` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE IF NOT EXISTS `tahun` (
`ID` int(11) NOT NULL,
  `TAHUN` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`ID`, `TAHUN`) VALUES
(2, '2014/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tutorial`
--

CREATE TABLE IF NOT EXISTS `tutorial` (
`ID_TUTORIAL` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL,
  `NAMA_TUTORIAL` varchar(50) NOT NULL,
  `NAMA_FILE` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` char(7) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
(1, '12345', '$2y$10$VWnjlxkJBaIqvRn00TkjcuxCvVVKe1aeBHU0Wqni4xHpBF5b/Yaq6', 1),
(3, '796976', '$2y$10$VIDNiaflq6.pDBeEfHlde..3NGwm2mVPD.rse0WVd6dJndu2FiXa2', 2),
(4, '575567', '$2y$10$X5j83MAFLuAxO93jqtOElewZcZ3lmQ7Spq426fYu6Zj4kAIVb7.2i', 3),
(5, '68565', '$2y$10$WXHs4vT1V.LfKrQQv6lyYu.Epw4AVxizhgPORHPX61E7sD68yOivq', 3),
(6, '78686', '$2y$10$ih7U8Cn14SZbplCXkzWUQOEmw7aC6kxwFJg73pVu40t3pYELBDgBW', 3),
(7, '56858', '$2y$10$WuxVE8v5h/PwVuBUc44lC.00B/hhka8VpyGMNF/W7oPzsr12DPLsi', 3),
(8, '56858', '$2y$10$//X3dQBZ/CrKmxgUPZz7NewCSAgqigHD3EqMYouwVROv1cujxbp82', 3),
(9, '56858', '$2y$10$HUWPionO9ea5lU2MF4U9juePic51/VWgaW/0JwDBtwJ03GzQUq2va', 3),
(10, '56858', '$2y$10$yVe83IqsfOUVWImfIA50..sfO9XPkIWjg7YcBaw2XdbLltCPjdg5y', 3),
(11, '56858', '$2y$10$lXarG6vRVdXfvc.QHlDQIeeTNMt.BVPPn4pcerLg3eCr/9e1GGGFW', 3);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
`ID_VIDEO` int(11) NOT NULL,
  `KODE_KELAS` char(7) NOT NULL,
  `NAMA_VIDEO` varchar(50) NOT NULL,
  `LINK_VIDEO` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
 ADD PRIMARY KEY (`ID`,`KODE_DOSEN`), ADD KEY `FK_RELATIONSHIP_18` (`USER_ID`);

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
 ADD PRIMARY KEY (`ID_FORUM`), ADD KEY `FK_RELATIONSHIP_3` (`KODE_KELAS`);

--
-- Indexes for table `hasil_quiz`
--
ALTER TABLE `hasil_quiz`
 ADD PRIMARY KEY (`ID_QUIZ`,`NRP`), ADD KEY `FK_RELATIONSHIP_14` (`NRP`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
 ADD PRIMARY KEY (`ID_INFORMASI`), ADD KEY `FK_RELATIONSHIP_16` (`KODE_KELAS`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
 ADD PRIMARY KEY (`KODE_KELAS`), ADD KEY `FK_RELATIONSHIP_2` (`KODE_MATAKULIAH`), ADD KEY `FK_RELATIONSHIP_5` (`KODE_DOSEN`);

--
-- Indexes for table `kelas_mahasiswa`
--
ALTER TABLE `kelas_mahasiswa`
 ADD PRIMARY KEY (`NRP`,`KODE_KELAS`), ADD KEY `FK_RELATIONSHIP_11` (`KODE_KELAS`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
 ADD PRIMARY KEY (`ID_KOMENTAR`), ADD KEY `FK_RELATIONSHIP_4` (`ID_FORUM`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
 ADD PRIMARY KEY (`ID`,`NRP`), ADD KEY `FK_RELATIONSHIP_17` (`USER_ID`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
 ADD PRIMARY KEY (`ID`,`KODE_MATAKULIAH`), ADD KEY `FK_RELATIONSHIP_1` (`ID_TAHUN`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
 ADD PRIMARY KEY (`ID_QUIZ`), ADD KEY `FK_RELATIONSHIP_8` (`KODE_KELAS`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
 ADD PRIMARY KEY (`ID_SOAL`), ADD KEY `FK_RELATIONSHIP_9` (`ID_QUIZ`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tutorial`
--
ALTER TABLE `tutorial`
 ADD PRIMARY KEY (`ID_TUTORIAL`), ADD KEY `FK_RELATIONSHIP_7` (`KODE_KELAS`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`,`username`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
 ADD PRIMARY KEY (`ID_VIDEO`), ADD KEY `FK_RELATIONSHIP_6` (`KODE_KELAS`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
MODIFY `ID_FORUM` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
MODIFY `ID_INFORMASI` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
MODIFY `ID_KOMENTAR` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
MODIFY `ID_QUIZ` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
MODIFY `ID_SOAL` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tutorial`
--
ALTER TABLE `tutorial`
MODIFY `ID_TUTORIAL` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
MODIFY `ID_VIDEO` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dosen`
--
ALTER TABLE `dosen`
ADD CONSTRAINT `FK_RELATIONSHIP_18` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `forum`
--
ALTER TABLE `forum`
ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`);

--
-- Constraints for table `hasil_quiz`
--
ALTER TABLE `hasil_quiz`
ADD CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`NRP`) REFERENCES `mahasiswa` (`ID`),
ADD CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`ID_QUIZ`) REFERENCES `quiz` (`ID_QUIZ`);

--
-- Constraints for table `information`
--
ALTER TABLE `information`
ADD CONSTRAINT `FK_RELATIONSHIP_16` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`);

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`KODE_MATAKULIAH`) REFERENCES `matakuliah` (`ID`),
ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`KODE_DOSEN`) REFERENCES `dosen` (`ID`);

--
-- Constraints for table `kelas_mahasiswa`
--
ALTER TABLE `kelas_mahasiswa`
ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`),
ADD CONSTRAINT `FK_RELATIONSHIP_12` FOREIGN KEY (`NRP`) REFERENCES `mahasiswa` (`ID`);

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_FORUM`) REFERENCES `forum` (`ID_FORUM`);

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
ADD CONSTRAINT `FK_RELATIONSHIP_17` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `matakuliah`
--
ALTER TABLE `matakuliah`
ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`ID_TAHUN`) REFERENCES `tahun` (`ID`);

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`);

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ID_QUIZ`) REFERENCES `quiz` (`ID_QUIZ`);

--
-- Constraints for table `tutorial`
--
ALTER TABLE `tutorial`
ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`);

--
-- Constraints for table `video`
--
ALTER TABLE `video`
ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`KODE_KELAS`) REFERENCES `kelas` (`KODE_KELAS`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
