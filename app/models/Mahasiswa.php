<?php

class Mahasiswa extends Eloquent {

	protected $table = 'mahasiswa';
	protected $fillable = array('ID','NRP','USER_ID', 'NAMA_MAHASISWA', 'TAHUN_MASUK');

	
	public static function data()
	{		
		return Mahasiswa::all();

	}

	public static function getMhsById($id){
		return Mahasiswa::find($id);
	}

	public static function tambah($nrp, $nama_mahasiswa, $user_id, $tahun_masuk)
	{
		// Mahasiswa::create(compact('nim', 'nama',  'fakultas', 'prodi', 'hash_password'));
		DB::table('mahasiswa')->insert(compact('nrp', 'nama_mahasiswa', 'user_id', 'tahun_masuk'));
	}

	public static function edit($id, $nim, $nama_mahasiswa, $tahun_masuk)
	{
		// $mhs = Mahasiswa::find($id);
		// $mhs->nim = $nim;
		// $mhs->nama = $nama;
		// $mhs->fakultas = $fakultas;
		// $mhs->prodi = $prodi;
		// $mhs->password = Hash::make($password);
		// $mhs->save();
		DB::table('mahasiswa')->where('ID', $id)->update(compact('nrp', 'nama_mahasiswa', 'tahun_masuk'));
	}

	public static function hapus($id)
	{
		// User::destroy($id);
		DB::table('mahasiswa')->where('ID','=', $id)->delete();
	}

}