<?php

class Dosen extends Eloquent {

	protected $table = 'dosen';
	protected $fillable = array('KODE_DOSEN','USERNAME','NAMA_DOSEN');

	
	public static function data()
	{		
		return Dosen::all();

	}

	public static function getDosenById($id){
		return Dosen::find($id);
	}

	public static function getDosenByKodeDosen($kode_dosen)
	{
		return Dosen::where('kode_dosen', '=', $kode_dosen)->get()->first();
	}

	public static function getDosenByUserId($user_id)
	{
		return Dosen::where('user_id', '=', $user_id)->get()->first();
	}

	public static function tambah($kode_dosen, $user_id, $nama_dosen)
	{
		// $hash_password = Hash::make($password);
		// Dosen::create(compact('nip', 'nama', 'password'));
		DB::table('dosen')->insert(compact('kode_dosen', 'user_id', 'nama_dosen'));
	}

	public static function edit($kode_dosen, $user_id, $nama_dosen)
	{
		// $dosen = Dosen::find($id);
		// $dosen->nip = $nip;
		// $dosen->nama = $nama;
		// $dosen->password = $password;
		// $dosen->save();
		DB::table('dosen')->where('KODE_DOSEN', $kode_dosen)->update(compact('kode_dosen','user_id','nama_dosen'));
	}

	public static function hapus($id)
	{
		// User::destroy($id);
		DB::table('dosen')->where('ID','=', $id)->delete();
	}

}