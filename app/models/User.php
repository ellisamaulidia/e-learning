<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	protected $fillable = array('USERNAME', 'PASSWORD', 'ROLE');
	protected $guarded = array('ID');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	public static function getUserById($id){
		return User::find($id);
	}

	public static function getUserByUsername($username)
	{
		return User::where('username', '=', $username)->get()->first();
	}
	
	public static function tambah($username, $password, $role)
	{
		// User::create(compact('username', 'password', 'role'));
		return DB::table('user')->insertGetId(compact('username','password', 'role'));
	}
	
	public static function edit($id, $username, $password, $role)
	{
		// $user = User::find($id);
		// $user->username = $username;
		// $user->password = $password;
		// $user->role = $role;
		// $user->save();
		DB::table('user')->where('ID', $id)->update(compact('username', 'password', 'role'));
	}

	public static function hapus($id)
	{
		// User::destroy($id);
		DB::table('user')->where('ID','=', $id)->delete();
	}
}
