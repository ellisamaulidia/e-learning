<?php

class Materi extends Eloquent {

	protected $table = 'materi';
	protected $fillable = array('id','filename','original_filename', 'deskripsi', 'id_matkul');

	
	public static function data()
	{		
		return Materi::all();
	}

	public static function getMateriById($id){
		return Materi::find($id);
	}

	public static function getMateriByMatkul($matkul){
		return Materi::where('id_matkul', '=', $matkul)->get();
	}

	public static function tambah($filename, $original_filename, $mime, $deskripsi, $id_matkul)
	{
		Materi::create(compact('filename', 'original_filename', 'mime', 'deskripsi', 'id_matkul'));
	}

	public static function edit($id, $filename, $original_filename, $deskripsi, $id_matkul, $mime)
	{
		$materi = Materi::find($id);
		$materi->filename = $filename;
		$materi->original_filename = $original_filename;
		$materi->deskripsi = $deskripsi;
		$materi->id_matkul = $id_matkul;
		$materi->mime = $mime;
		$materi->save();
	}

	public static function hapus($id)
	{
		Materi::destroy($id);
	}

}