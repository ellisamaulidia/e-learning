<?php

class Matakuliah extends Eloquent {

	protected $table = 'matakuliah';
	protected $fillable = array('id','kode_matakuliah', 'id_tahun','matakuliah', 'sks', 'deskripsi');

	
	public static function data()
	{		
		return Matakuliah::all();

	}

	public static function getMatkulById($id){
		return Matakuliah::find($id);
	}

	public static function getMatkulByTahun($tahun){
		return Matakuliah::where('id_tahun', '=', $tahun)->get();
	}

	public static function getMatkulByDosen($dosen){
		return Matakuliah::where('dosen', '=', $dosen)->get();
	}

	public static function tambah($kode_matakuliah, $id_tahun, $matakuliah, $sks, $deskripsi)
	{
		// Matakuliah::create(compact('tahun_ajar', 'nama_matkul', 'sks', 'dosen'));
		DB::table('matakuliah')->insert(compact('kode_matakuliah','id_tahun', 'matakuliah', 'sks', 'deskripsi'));
	}

	public static function edit($id, $kode_matakuliah, $matakuliah, $id_tahun, $sks, $deskripsi)
	{
		// $matkul = Matakuliah::find($id);
		// $matkul->tahun_ajar = $matkul->tahun_ajar;
		// $matkul->nama_matkul = $nama_matkul;
		// $matkul->sks = $sks;
		// $matkul->dosen = $dosen;
		// $matkul->save();
		DB::table('matakuliah')->where('ID', $id)->update(compact('kode_matakuliah','matakuliah', 'id_tahun', 'sks', 'deskripsi'));
	}

	public static function hapus($id)
	{
		// User::destroy($id);
		DB::table('matakuliah')->where('ID','=', $id)->delete();
	}

}