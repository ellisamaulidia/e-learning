<?php

class Tahun extends Eloquent {

	protected $table = 'tahun';
	protected $fillable = array('id','tahun');

	
	public static function data()
	{		
		return Tahun::all();

	}

	public static function getTahunById($id){
		return Tahun::find($id);
	}

	public static function tambah($tahun)
	{
		// Tahun::create(compact('tahun'));
		DB::table('tahun')->insert(compact('tahun'));
	}

	public static function edit($id, $tahun)
	{
		// $tahun = Tahun::find($id);
		// $tahun->tahun = $thn;
		// $tahun->save();
		DB::table('tahun')->where('ID', $id)->update(compact('tahun'));
	}

	public static function hapus($id)
	{
		// Tahun::destroy($id);
		DB::table('tahun')->where('ID','=', $id)->delete();
	}

}