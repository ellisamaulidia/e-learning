<?php

class MahasiswaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function data()
	{
		// data
		$mahasiswa = Mahasiswa::data();
		
		return View::make('pages.admin.mahasiswa', compact('mahasiswa'));
	}

	public function getTambah()
	{
		
		return View::make('modals.tambah_mhs');
	}

	public function postTambah()
	{
		// validasi
		$input = Input::all();
		$rules = array(
			'nama' => 'required|max:255',
			'nrp' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nama = $validasi->messages()->first('nama') ?: '';
			$status = '';

			return Response::json(compact('nama', 'status'));

		// valid
		} else {
			
			$nrp = Input::get('nrp');
			$nama = Input::get('nama');
			$thn_masuk = Input::get('thn_masuk');
			$password = md5(Input::get('password'));

			$id = User::tambah($nrp, $password, 3);
			Mahasiswa::tambah($nrp, $nama, $id, $thn_masuk);
		}
	}

	public function getEdit($id)
	{
		$mhs = Mahasiswa::getMhsById($id);
		$user = User::getUserById($mhs->USER_ID);

		return View::make('modals.edit_mhs', compact('mhs', 'user'));
	}

	public function postEdit($id)
	{
		// data
		$mhs = Mahasiswa::getMhsById($id);
		$user = User::getUserById($mhs->USER_ID);

		// validasi
		$input = Input::all();
		$rules = array(
			'nrp' => 'required|max:255',
			'nama' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nrp = $validasi->messages()->first('nrp') ?: '';
			$nama = $validasi->messages()->first('nama') ?: '';
			$status = '';

			return Response::json(compact('nrp', 'nama', 'thn_masuk', 'password' , 'status'));

		// valid
		} else {

			// input
			$nrp = trim(ucwords(Input::get('nrp')));
			$nama = trim(ucwords(Input::get('nama')));
			$thn_masuk = Input::get('thn_masuk');
			$password = md5(Input::get('password'));

			// ubabh data di basisdata
			User::edit($user->ID, $nrp, $password, 3);
			Mahasiswa::edit($id, $nrp, $nama, $thn_masuk);
		}
	}

	public function getHapus($id)
	{
		$mhs = Mahasiswa::getMhsById($id);

		return View::make('modals.hapus_mhs', compact('mhs'));
	}

	public function postHapus($id)
	{
		$mhs = Mahasiswa::getMhsById($id);
		$user = User::getUserById($mhs->USER_ID);
		User::hapus($user->ID);
		Mahasiswa::hapus($id);
	}

}
