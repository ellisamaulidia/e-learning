<?php

class TahunAjarController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function data()
	{
		// data
		$tahun = Tahun::data();
		
		return View::make('pages.admin.tahun', compact('tahun'));
	}

	public function getTambah()
	{
		
		return View::make('modals.tambah_tahun');
	}

	public function postTambah()
	{
		// validasi
		$input = Input::all();
		$rules = array(
			'tahun' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nama = $validasi->messages()->first('tahun') ?: '';
			$status = '';

			return Response::json(compact('tahun', 'status'));

		// valid
		} else {
			
			$tahun = Input::get('tahun');

			Tahun::tambah($tahun);
		}
	}

	public function getEdit($id)
	{
		$tahun = Tahun::getTahunById($id);

		return View::make('modals.edit_tahun', compact('tahun'));
	}

	public function postEdit($id)
	{
		// data
		$tahun = Tahun::getTahunById($id);

		// validasi
		$input = Input::all();
		$rules = array(
			'tahun' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$tahun = $validasi->messages()->first('tahun') ?: '';
			$status = '';

			return Response::json(compact('tahun','status'));

		// valid
		} else {

			// input
			$tahun = trim(ucwords(Input::get('tahun')));

			// ubabh data di basisdata
			Tahun::edit($id, $tahun);
		}
	}

	public function getHapus($id)
	{
		$tahun = Tahun::getTahunById($id);

		return View::make('modals.hapus_tahun', compact('tahun'));
	}

	public function postHapus($id)
	{
		$matkul = Matakuliah::getMatkulByTahun($id);
		foreach ($matkul as $mk) {
			Matakuliah::hapus($mk->ID);
		}
		Tahun::hapus($id);
	}

}
