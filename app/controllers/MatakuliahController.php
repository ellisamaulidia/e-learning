<?php

class MatakuliahController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function data($id)
	{
		// data
		$tahun = Tahun::getTahunById($id);
		$matkul = Matakuliah::getMatkulByTahun($tahun->ID);
		
		return View::make('pages.admin.matakuliah', compact('tahun', 'matkul'));
	}

	public function getTambah($id)
	{
		$tahun = Tahun::getTahunById($id);
		return View::make('modals.tambah_matkul', compact('tahun'));
	}

	public function postTambah($id)
	{
		// validasi
		$input = Input::all();
		$rules = array(
			'nama_matkul' => 'required|max:255',
			'sks' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nama_matkul = $validasi->messages()->first('nama_matkul') ?: '';
			$sks = $validasi->messages()->first('sks') ?: '';
			$status = '';

			return Response::json(compact('nama_matkul', 'sks', 'status'));

		// valid
		} else {
			
			$tahun = $id;
			$kode_matkul = Input::get('kode_matkul');
			$nama_matkul = Input::get('nama_matkul');
			$sks = Input::get('sks');
			$deskripsi = Input::get('deskripsi');
			// return Response::json(compact('nama_matkul', 'sks', 'dosen'));
			Matakuliah::tambah($kode_matkul, $tahun, $nama_matkul, $sks, $deskripsi);
		}
	}

	public function getEdit($id)
	{
		$matkul = Matakuliah::getMatkulById($id);
		$dosen = Dosen::data();

		return View::make('modals.edit_matkul', compact('matkul', 'dosen'));
	}

	public function postEdit($id)
	{
		// data
		$matkul = Matakuliah::getMatkulById($id);

		// validasi
		$input = Input::all();
		$rules = array(
			'nama_matkul' => 'required|max:255',
			'sks' => 'required|numeric'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nama_matkul = $validasi->messages()->first('nama_matkul') ?: '';
			$sks = $validasi->messages()->first('sks') ?: '';
			$status = '';

			return Response::json(compact('nama_matkul', 'sks', 'status'));

		// valid
		} else {

			// input
			$nama_matkul = Input::get('nama_matkul');
			$kode_matkul = Input::get('kode_matkul');
			$sks = Input::get('sks');
			$deskripsi = Input::get('deskripsi');

			// ubabh data di basisdata
			Matakuliah::edit($id, $kode_matkul, $nama_matkul, $matkul->ID_TAHUN, $sks, $deskripsi);
		}
	}

	public function getHapus($id)
	{
		$matkul = Matakuliah::getMatkulById($id);

		return View::make('modals.hapus_matkul', compact('matkul'));
	}

	public function postHapus($id)
	{
		Matakuliah::hapus($id);
	}

}
