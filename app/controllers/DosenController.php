<?php

class DosenController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function data()
	{
		// data
		$dosen = Dosen::data();
		
		return View::make('pages.admin.dosen', compact('dosen'));
	}

	public function getTambah()
	{
		
		return View::make('modals.tambah_dosen');
	}

	public function postTambah()
	{
		// validasi
		$input = Input::all();
		$rules = array(
			'nama' => 'required|max:255',
			'nip' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nama = $validasi->messages()->first('nama') ?: '';
			$status = '';

			return Response::json(compact('nama', 'status'));

		// valid
		} else {
			
			$kode_dosen = Input::get('nip');
			$nama_dosen = Input::get('nama');
			$password = md5(Input::get('password'));

			$id = User::tambah($kode_dosen, $password, 2);
			// $id = DB::Query('SELECT LAST_INSERT_ID()');
			Dosen::tambah($kode_dosen, $id, $nama_dosen);
			// return Response::json(compact('id'));
		}
	}

	public function getEdit($id)
	{
		$dosen = Dosen::getDosenById($id);
		$user = User::getUserById($dosen->USER_ID);

		return View::make('modals.edit_dosen', compact('dosen','user'));
	}

	public function postEdit($id)
	{
		// data
		$dosen = Dosen::getDosenById($id);
		$user = User::getUserById($dosen->USER_ID);

		// validasi
		$input = Input::all();
		$rules = array(
			'nip' => 'required|max:255',
			'nama' => 'required|max:255',
			'password' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nip = $validasi->messages()->first('nip') ?: '';
			$nama = $validasi->messages()->first('nama') ?: '';
			$password = $validasi->messages()->first('password') ?: '';
			$status = '';

			return Response::json(compact('nip', 'nama', 'password', 'status'));

		// valid
		} else {

			// input
			$kode_dosen = trim(ucwords(Input::get('nip')));
			$nama_dosen = trim(ucwords(Input::get('nama')));
			$password = md5(Input::get('password'));

			// ubah data di basisdata
			// var_dump(User::edit($user->ID, $kode_dosen, $password, 2));die;
			User::edit($user->ID, $kode_dosen, $password, 2);
			Dosen::edit($kode_dosen, $user->ID, $nama_dosen);
			
		}
	}

	public function getHapus($id)
	{
		$dosen = Dosen::getDosenById($id);

		return View::make('modals.hapus_dosen', compact('dosen'));
	}

	public function postHapus($id)
	{
		$dosen = Dosen::getDosenById($id);
		$user = User::getUserById($dosen->USER_ID);
		// var_dump(Dosen::hapus($id));die;
		User::hapus($user->ID);
		Dosen::hapus($id);
	}

}
