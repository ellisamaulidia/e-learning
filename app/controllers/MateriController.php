<?php

class MateriController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function data()
	{
		// data
		$dosen = Dosen::getDosenByNip(Auth::user()->username);
		$matkul = new Matakuliah();
		$matkul = Matakuliah::getMatkulByDosen($dosen->id);
		// dd($dosen);
		
		return View::make('pages.dosen.materi', compact('matkul'));
	}

	public function getTambah()
	{
		$dosen = Dosen::getDosenByNip(Auth::user()->username);
		$matkul = Matakuliah::getMatkulByDosen($dosen->id);
		return View::make('modals.tambah_materi', compact('matkul'));
	}

	public function postTambah()
	{
		// validasi	

		$input = Input::all();
		$rules = array(
			'matkul' => 'required|max:255',
			'deskripsi' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$matkul = $validasi->messages()->first('matkul') ?: '';
			$deskripsi = $validasi->messages()->first('deskripsi') ?: '';
			$status = '';

			return Response::json(compact('matkul','deskripsi', 'status'));

		// valid
		} else {

			$file_upload = Input::file('file_materi');
			$original_name = $file_upload->getClientOriginalName();

			Input::file('file_materi')->move(storage_path() . '/uploads', $original_name);
			
			$matkul = Input::get('matkul');
			$deskripsi = Input::get('deskripsi');
			$filename = $file_upload->getFileName();
			$mimeType = $file_upload->getClientMimeType();

			Materi::tambah($filename, $original_name, $mimeType, $deskripsi, $matkul);
		}
	}

	public function getEdit($id)
	{
		$dosen = Dosen::getDosenByNip(Auth::user()->username);
		$matkul = Matakuliah::getMatkulByDosen($dosen->id);
		$materi = Materi::getMateriById($id);

		return View::make('modals.edit_materi', compact('matkul', 'materi'));
	}

	public function postEdit($id)
	{
		// validasi
		$input = Input::all();
		$rules = array(
			'nama_matkul' => 'required|numeric',
			'deskripsi' => 'required|max:255'
		);
		$validasi = Validator::make(Input::all(), $rules);

		// tidak valid
		if ($validasi->fails()) {
			// respon
			$nip = $validasi->messages()->first('nama_matkul') ?: '';
			$nama = $validasi->messages()->first('deskripsi') ?: '';
			$status = '';

			return Response::json(compact('nama_matkul', 'deskripsi'));

		// valid
		} else {

			// input
			$file_upload = Input::file('file_materi');
			$original_name = $file_upload->getClientOriginalName();

			Input::file('file_materi')->move(storage_path() . '/uploads', $original_name);
			
			$matkul = Input::get('matkul');
			$deskripsi = Input::get('deskripsi');
			$filename = $file_upload->getFileName();
			$mimeType = $file_upload->getClientMimeType();

			// ubah data di basisdata
			Materi::edit($id, $filename, $original_name, $deskripsi, $id_matkul, $mimeType);
		}
	}

	public function getHapus($id)
	{
		$materi = Materi::getMateriById($id);

		return View::make('modals.hapus_materi', compact('materi'));
	}

	public function postHapus($id)
	{
		Materi::hapus($id);
	}

}
