<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'AdminController@home'));

Route::get('login', array('as' => 'login', 'uses' => 'AdminController@getLogin'));
Route::post('login', array('uses' => 'AdminController@postLogin'));

Route::get('tahun', array('as' => 'data_tahun', 'uses' => 'TahunAjarController@data'));
Route::get('tahun/tambah', array('as' => 'tambah_tahun', 'uses' => 'TahunAjarController@getTambah'));
Route::post('tahun/tambah', array('uses' => 'TahunAjarController@postTambah'));
Route::get('tahun/edit/{id?}', array('as' => 'edit_tahun', 'uses' => 'TahunAjarController@getEdit'))->where('id', '\d+');
Route::post('tahun/edit/{id?}', array('uses' => 'TahunAjarController@postEdit'))->where('id', '\d+');
Route::get('tahun/hapus/{id?}', array('as' => 'hapus_tahun', 'uses' => 'TahunAjarController@getHapus'))->where('id', '\d+');
Route::post('tahun/hapus/{id?}', array('uses' => 'TahunAjarController@postHapus'))->where('id', '\d+');

Route::get('matkul/{id?}', array('as' => 'data_matkul', 'uses' => 'MatakuliahController@data'))->where('id', '\d+');
Route::get('matkul/tambah/{id?}', array('as' => 'tambah_matkul', 'uses' => 'MatakuliahController@getTambah'))->where('id', '\d+');
Route::post('matkul/tambah/{id?}', array('uses' => 'MatakuliahController@postTambah'))->where('id', '\d+');
Route::get('matkul/edit/{id?}', array('as' => 'edit_matkul', 'uses' => 'MatakuliahController@getEdit'))->where('id', '\d+');
Route::post('matkul/edit/{id?}', array('uses' => 'MatakuliahController@postEdit'))->where('id', '\d+');
Route::get('matkul/hapus/{id?}', array('as' => 'hapus_matkul', 'uses' => 'MatakuliahController@getHapus'))->where('id', '\d+');
Route::post('matkul/hapus/{id?}', array('uses' => 'MatakuliahController@postHapus'))->where('id', '\d+');

Route::get('mhs', array('as' => 'data_mhs', 'uses' => 'MahasiswaController@data'));
Route::get('mhs/tambah', array('as' => 'tambah_mhs', 'uses' => 'MahasiswaController@getTambah'));
Route::post('mhs/tambah', array('uses' => 'MahasiswaController@postTambah'));
Route::get('mhs/edit/{id?}', array('as' => 'edit_mhs', 'uses' => 'MahasiswaController@getEdit'))->where('id', '\d+');
Route::post('mhs/edit/{id?}', array('uses' => 'MahasiswaController@postEdit'))->where('id', '\d+');
Route::get('mhs/hapus/{id?}', array('as' => 'hapus_mhs', 'uses' => 'MahasiswaController@getHapus'))->where('id', '\d+');
Route::post('mhs/hapus/{id?}', array('uses' => 'MahasiswaController@postHapus'))->where('id', '\d+');

Route::get('dosen', array('as' => 'data_dosen', 'uses' => 'DosenController@data'));
Route::get('dosen/tambah', array('as' => 'tambah_dosen', 'uses' => 'DosenController@getTambah'));
Route::post('dosen/tambah', array('uses' => 'DosenController@postTambah'));
Route::get('dosen/edit/{KODE_DOSEN?}', array('as' => 'edit_dosen', 'uses' => 'DosenController@getEdit'))->where('KODE_DOSEN', '\d+');
Route::post('dosen/edit/{KODE_DOSEN?}', array('uses' => 'DosenController@postEdit'))->where('KODE_DOSEN', '\d+');
Route::get('dosen/hapus/{id?}', array('as' => 'hapus_dosen', 'uses' => 'DosenController@getHapus'))->where('id', '\d+');
Route::post('dosen/hapus/{id?}', array('uses' => 'DosenController@postHapus'))->where('id', '\d+');

Route::get('materi', array('as' => 'data_materi', 'uses' => 'MateriController@data'));
Route::get('materi/tambah', array('as' => 'tambah_materi', 'uses' => 'MateriController@getTambah'));
Route::post('materi/tambah', array('uses' => 'MateriController@postTambah'));
Route::get('materi/edit/{id?}', array('as' => 'edit_materi', 'uses' => 'MateriController@getEdit'))->where('id', '\d+');
Route::post('materi/edit/{id?}', array('uses' => 'MateriController@postEdit'))->where('id', '\d+');
Route::get('materi/hapus/{id?}', array('as' => 'hapus_materi', 'uses' => 'MateriController@getHapus'))->where('id', '\d+');
Route::post('materi/hapus/{id?}', array('uses' => 'MateriController@postHapus'))->where('id', '\d+');

Route::get('logout', array('as' => 'logout', 'uses' => 'AdminController@logout'));