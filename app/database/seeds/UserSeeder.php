<?php
/**
* 
*/
class UserSeeder extends Seeder
{

	public function run()
	{
		$user = array(
			'username' => '12345',
			'password' => Hash::make('12345'),
			'role' => 1
		);
		DB::table('user')->insert($user);
	}
}