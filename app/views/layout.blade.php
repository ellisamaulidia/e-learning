<!DOCTYPE html>
<html>

  @include('head')

  <body class="skin-blue">
    <div class="wrapper">
      

      <?php $user = Auth::user();
        $username = '';
        $userdata = new Dosen();
        if($user->role == 1){
          $username = 'Admin';
        } elseif ($user->role == 2) {
          $userdata = Dosen::getDosenByUserId($user->id);
          $username = $userdata->NAMA_DOSEN;
        }
      ?>

      @include('header')

      <!-- Left side column. contains the logo and sidebar -->
      <?php 
        if($user->role == 1){ ?>
          @include('sidebar_left')
        <?php
        } elseif ($user->role == 2) { ?>
          @include('sidebar_left_dosen')
        <?php
        }
      ?>
      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        @include('home')

      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    @include('script_js')

    @include('url')

  </body>
</html>