<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{URL::to('/')}}">
          <i class="fa fa-dashboard"></i> <span>Home</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="dataMateri()" title="Materi Kuliah" id="link-materi">
          <i class="fa fa-files-o"></i>
          <span>Materi Kuliah</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="" title="Data Mahasiswa" id="link-informasi">
          <i class="fa fa-th"></i> <span>Informasi</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="" title="Quiz" id="link-quiz">
          <i class="fa fa-pie-chart"></i>
          <span>Quiz</span>
        </a>              
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>