  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Quiz
      <small>Master</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Quiz</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Quiz</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kelas</th>
                    <th>Nama Quiz</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($matkul as $mk)
                    <?php 
                      $materi = Materi::getMateriByMatkul($mk->id);
                    ?>
                    @foreach($materi as $mtr)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $mk->nama_matkul }}</td>
                      <td>{{ $mtr->original_filename }}</td>
                      <td>{{ $mtr->deskripsi }}</td>
                      <td> 
                        {{ Form::button('<i class="fa fa-pencil"></i>', array('class' => 'btn btn-warning  btn-xs', 'onclick' => "modalEditMateri($mtr->id)", 'title' => 'Ubah')) }}

                        {{ Form::button('<i class="fa fa-trash-o "></i>', array('class' => 'btn btn-danger btn-xs', 'onclick' => "modalHapusMateri($mtr->id)", 'title' => 'Hapus')) }}
                      
                      </td>
                    </tr>
                      <?php $i++; ?>
                      @endforeach
                    @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Kelas</th>
                    <th>Nama Quiz</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->          
          </div><!-- /.box -->
          <div class="pull-right box-tools">
            {{ Form::button('Tambah', array('class' => 'btn btn-primary', 'onclick' => "modalTambahMateri()")) }}
          </div>
      </div><!-- ./col -->
    </div><!-- /.row -->

  </section><!-- /.content -->

 