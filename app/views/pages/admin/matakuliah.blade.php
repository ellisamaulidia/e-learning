  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Mata Kuliah
      <small>Master</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mata Kuliah</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ $tahun->TAHUN }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Mata Kuliah</th>
                    <th>Mata Kuliah</th>
                    <th>SKS</th>
                    <th>Deskripsi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($matkul as $mk)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $mk->KODE_MATAKULIAH }}</td>
                      <td>{{ $mk->MATAKULIAH }}</td>
                      <td>{{ $mk->SKS }}</td>
                      <td>{{ $mk->DESKRIPSI }}</td>
                      <td> 
                        {{ Form::button('<i class="fa fa-pencil"></i>', array('class' => 'btn btn-warning  btn-xs', 'onclick' => "modalEditMatkul($mk->ID)", 'title' => 'Ubah')) }}

                        {{ Form::button('<i class="fa fa-trash-o "></i>', array('class' => 'btn btn-danger btn-xs', 'onclick' => "modalHapusMatkul($mk->ID)", 'title' => 'Hapus')) }}
                      
                      </td>
                    </tr>
                    <?php $i++; ?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Kode Mata Kuliah</th>
                    <th>Mata Kuliah</th>
                    <th>SKS</th>
                    <th>Deskripsi</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->          
          </div><!-- /.box -->
          <div class="pull-right box-tools">
            {{ Form::button('Tambah', array('class' => 'btn btn-primary', 'onclick' => "modalTambahMatkul($tahun->ID)")) }}
          </div>
      </div><!-- ./col -->
    </div><!-- /.row -->

  </section><!-- /.content -->

 