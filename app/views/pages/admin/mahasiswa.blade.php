  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>DATA MAHASISWA
      <small>Master</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mahasiswa</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Mahasiwa</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>Tahun Masuk</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($mahasiswa as $mhs)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $mhs->NRP }}</td>
                      <td>{{ $mhs->NAMA_MAHASISWA }}</td>
                      <td>{{ $mhs->TAHUN_MASUK }}</td>
                      <td> 
                        {{ Form::button('<i class="fa fa-pencil"></i>', array('class' => 'btn btn-warning  btn-xs', 'onclick' => "modalEditMhs($mhs->ID)", 'title' => 'Ubah')) }}

                        {{ Form::button('<i class="fa fa-trash-o "></i>', array('class' => 'btn btn-danger btn-xs', 'onclick' => "modalHapusMhs($mhs->ID)", 'title' => 'Hapus')) }}
                      
                      </td>
                    </tr>
                    <?php $i++; ?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>Tahun Masuk</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->          
          </div><!-- /.box -->
          <div class="pull-right box-tools">
            <button class="btn btn-primary right" onclick="modalTambahMhs()">Tambah</button>
          </div>
      </div><!-- ./col -->
    </div><!-- /.row -->

  </section><!-- /.content -->

 