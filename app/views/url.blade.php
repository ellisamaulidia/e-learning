<script type="text/javascript">
	var url_home = '{{ route('home') }}';
	var url_login = '{{ route('login') }}';

	var url_data_tahun = '{{  route('data_tahun') }}';
	var url_tambah_tahun = '{{  route('tambah_tahun') }}';
	var url_edit_tahun = '{{  route('edit_tahun') }}';
	var url_hapus_tahun = '{{  route('hapus_tahun') }}';

	var url_data_matkul = '{{  route('data_matkul') }}';
	var url_tambah_matkul = '{{  route('tambah_matkul') }}';
	var url_edit_matkul = '{{  route('edit_matkul') }}';
	var url_hapus_matkul = '{{  route('hapus_matkul') }}';

	var url_data_mhs = '{{  route('data_mhs') }}';
	var url_tambah_mhs = '{{  route('tambah_mhs') }}';
	var url_edit_mhs = '{{  route('edit_mhs') }}';
	var url_hapus_mhs = '{{  route('hapus_mhs') }}';

	var url_data_dosen = '{{  route('data_dosen') }}';
	var url_tambah_dosen = '{{  route('tambah_dosen') }}';
	var url_edit_dosen = '{{  route('edit_dosen') }}';
	var url_hapus_dosen = '{{  route('hapus_dosen') }}';

	var url_data_materi = '{{  route('data_materi') }}';
	var url_tambah_materi = '{{  route('tambah_materi') }}';
	var url_edit_materi = '{{  route('edit_materi') }}';
	var url_hapus_materi = '{{  route('hapus_materi') }}';

	var url_logout = '{{ route('logout') }}';
	
	var token = '{{ csrf_token() }}';
</script>