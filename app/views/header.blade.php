<header class="main-header">
  <!-- Logo -->
  <a href="index2.html" class="logo"><b>E-Learning</b></a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="user user-menu">
          <a href="#" onclick="logout()">Sign Out</a>
        </li>
      </ul>
    </div>
  </nav>
</header>