
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Mahasiswa</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('edit_mhs', $mhs->ID), 'files' => true, 'class' => 'form-horizontal form-edit-mhs')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">NRP</label>
                      <div class="col-lg-10">
                          <input type="text" name="nrp" class="form-control" id="nrp" value="<?= $mhs->NRP ?>">
                          <span class="help-block" id="error-nrp"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Nama</label>
                      <div class="col-lg-10">
                          <input type="text" name="nama" class="form-control" id="nama" value="<?= $mhs->NAMA_MAHASISWA ?>">
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Tahun Masuk</label>
                      <div class="col-lg-10">
                          <input type="text" name="thn_masuk" class="form-control" id="thn_masuk" value="<?= $mhs->TAHUN_MASUK ?>">
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Password</label>
                      <div class="col-lg-10">
                          <input type="text" name="password" class="form-control" id="password" value="<?= $user->PASSWORD ?>">
                      </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="editMhs()">Simpan</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>