
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Tahun</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('edit_tahun', $tahun->ID), 'files' => true, 'class' => 'form-horizontal form-edit-tahun')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Tahun</label>
                      <div class="col-lg-10">
                          <input type="text" name="tahun" class="form-control" id="tahun" value="<?= $tahun->TAHUN ?>">
                          <span class="help-block" id="error-tahun"></span>
                      </div>
                </div>          
            </div>
            <div class="modal-footer">                
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
                {{ Form::button('Simpan', array('class' => 'btn btn-primary', 'onclick' => "editTahun()")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>