
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Dosen</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => 'tambah_dosen', 'files' => true, 'class' => 'form-horizontal form-tambah-dosen')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Kode Dosen</label>
                      <div class="col-lg-10">
                          <input type="text" name="nip" class="form-control" id="nip" onkeypress="">
                          <span class="help-block" id="error-nim"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Nama</label>
                      <div class="col-lg-10">
                          <input type="text" name="nama" class="form-control" id="nama" onkeypress="">
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Password</label>
                      <div class="col-lg-10">
                          <input type="text" name="password" class="form-control" id="password" onkeypress="">
                      </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="tambahDosen()">Simpan</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>