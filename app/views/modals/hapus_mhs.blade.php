
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Hapus Mahasiswa</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus data mahasasiswa dengan NRP {{ $mhs->NRP }} ?</p>                
            </div>
            <div class="modal-footer">
                {{ Form::button('Hapus', array('class' => 'btn btn-primary', 'onclick' => "hapusMhs($mhs->ID)")) }}
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
            </div>
        </div>
    </div>
</div>