
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Matkul untuk Tahun Ajaran {{ $tahun->TAHUN }}</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('tambah_matkul', $tahun->ID), 'files' => true, 'class' => 'form-horizontal form-tambah-matkul')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Kode Matkul</label>
                      <div class="col-lg-10">
                          <input type="text" name="kode_matkul" class="form-control" id="kode_matkul">
                          <span class="help-block" id="error-kode_matkul"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Nama Matkul</label>
                      <div class="col-lg-10">
                          <input type="text" name="nama_matkul" class="form-control" id="nama_matkul">
                          <span class="help-block" id="error-nama_matkul"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">SKS</label>
                      <div class="col-lg-10">
                          <input type="text" name="sks" class="form-control" id="sks">
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Deskripsi</label>
                      <div class="col-lg-10">
                          <input type="text" name="deskripsi" class="form-control" id="deskripsi">
                          <span class="help-block" id="error-deskripsi"></span>
                      </div>
                </div>
                
            </div>
            <div class="modal-footer">
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
                {{ Form::button('Simpan', array('class' => 'btn btn-primary', 'onclick' => "tambahMatkul($tahun->ID)")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>