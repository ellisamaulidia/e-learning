
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Dosen</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('edit_dosen', $dosen->ID), 'files' => true, 'class' => 'form-horizontal form-edit-dosen')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Kode Dosen</label>
                      <div class="col-lg-10">
                          <input type="text" name="nip" class="form-control" id="nip" value="<?= $dosen->KODE_DOSEN ?>">
                          <span class="help-block" id="error-nip"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Nama</label>
                      <div class="col-lg-10">
                          <input type="text" name="nama" class="form-control" id="nama" value="<?= $dosen->NAMA_DOSEN ?>">
                      </div>
                </div> 
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Password</label>
                      <div class="col-lg-10">
                          <input type="text" name="password" class="form-control" id="password" value="<?= $user->PASSWORD ?>">
                      </div>
                </div>               
            </div>
            <div class="modal-footer">                
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
                {{ Form::button('Simpan', array('class' => 'btn btn-primary', 'onclick' => "editDosen()")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>