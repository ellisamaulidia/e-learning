
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Hapus Materi</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus data dosen dengan materi {{ $materi->original_filename }} ?</p>                
            </div>
            <div class="modal-footer">
                {{ Form::button('Hapus', array('class' => 'btn btn-primary', 'onclick' => "hapusMateri($materi->id)")) }}
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
            </div>
        </div>
    </div>
</div>