
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Materi</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('edit_materi', $materi->id), 'files' => true, 'class' => 'form-horizontal form-edit-materi')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Mata Kuliah</label>
                      <div class="col-lg-10">
                          <select class="form-control" name="matkul">
                            <option value="">- Pilih Mata Kuliah -</option>
                            @foreach($matkul as $mk)
                            <option value="{{ $mk->id }}" {{ ($mk->id == $materi->id_matkul) ? 'selected' : '' }} >{{ $mk->nama_matkul }}</option>
                            @endforeach
                          </select>                          
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">File</label>
                      <div class="col-lg-10">
                          <input type="file" name="file_materi" class="form-control" id="file_materi" value="<?= $mk->original_filename ?>">
                      </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label col-lg-2" for="">Deskripsi Materi</label>
                  <div class="col-lg-10">
                      <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?= $materi->deskripsi ?>">
                      <span class="help-block" id="error-deskripsi"></span>
                  </div>
                </div>
                
            </div>
            <div class="modal-footer">
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
                {{ Form::button('Simpan', array('class' => 'btn btn-primary', 'onclick' => "editMateri()")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>