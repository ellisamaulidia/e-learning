
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Mata Kuliah</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('route' => array('edit_matkul', $matkul->ID), 'files' => true, 'class' => 'form-horizontal form-edit-matkul')) }}
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Kode Matkul</label>
                      <div class="col-lg-10">
                          <input type="text" name="kode_matkul" class="form-control" id="kode_matkul" value="<?= $matkul->KODE_MATAKULIAH ?>">
                          <span class="help-block" id="error-nip"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Nama Matkul</label>
                      <div class="col-lg-10">
                          <input type="text" name="nama_matkul" class="form-control" id="nama_matkul" value="<?= $matkul->MATAKULIAH ?>">
                          <span class="help-block" id="error-nama_matkul"></span>
                      </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">SKS</label>
                      <div class="col-lg-10">
                          <input type="text" name="sks" class="form-control" id="sks" value="<?= $matkul->SKS ?>">
                      </div>
                </div> 
                <div class="form-group">
                      <label class="col-sm-2 control-label col-lg-2" for="">Deskripsi</label>
                      <div class="col-lg-10">
                          <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?= $matkul->DESKRIPSI ?>">
                      </div>
                </div>       
            </div>
            <div class="modal-footer">                
                {{ Form::button('Batal', array('class' => 'btn', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
                {{ Form::button('Simpan', array('class' => 'btn btn-primary', 'onclick' => "editMatkul($matkul->ID)")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>