<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <!-- <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
      </div> -->
      <div class="pull-left info">
        <p>{{ $username }}</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{URL::to('/')}}">
          <i class="fa fa-dashboard"></i> <span>Home</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="dataTahun()" title="Mata Kuliah" id="link-matakuliah">
          <i class="fa fa-book"></i>
          <span>Mata Kuliah</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="dataMahasiswa()" title="Data Mahasiswa" id="link-data_mahasiswa">
          <i class="fa fa-users"></i> <span>Mahasiswa</span>
        </a>
      </li>
      <li>
        <a href="#" onclick="dataDosen()" title="Data Dosen" id="link-data_dosen">
          <i class="fa fa-user"></i>
          <span>Dosen</span>
        </a>              
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>