<div class="konten">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Home
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Selamat Datang, {{ $username }} !</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            </div>
        </div>
      </div>
    </div><!-- /.row -->

  </section><!-- /.content -->
</div>

<div class="modalshow">
    
</div>